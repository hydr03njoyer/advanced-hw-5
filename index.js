const USERS_API = "https://ajax.test-danit.com/api/json/users";
const POSTS_API = "https://ajax.test-danit.com/api/json/posts";

class UserCard {
    constructor(post) {
        this.postId = post.id;
        this.userId = post.userId;
        this.title = post.title;
        this.text = post.body;
        this.userName = "";
        this.userEmail = "";
    }

    createCard() {
        const cardElement = document.createElement("div");

        cardElement.className = "card";
        cardElement.innerHTML = `
            <span class="delete-btn" data-post-id="${this.postId}">Unfollow</span>
            <h2>${this.title}</h2>
            <p>${this.text}</p>
            <p class="author">${this.userName} (${this.userEmail})</p>
        `;

        return cardElement;
    }
}

document.addEventListener("DOMContentLoaded", () => {
    const cardsContainer = document.querySelector(".container");

    fetch(USERS_API)
        .then(response => response.json())
        .then(usersData => {
            fetch(POSTS_API)
                .then(response => response.json())
                .then(postsData => {
                    postsData.forEach(post => {
                        const card = new UserCard(post);
                        const user = usersData.find(u => u.id === post.userId);
                        card.userName = user.name;
                        card.userEmail = user.email;

                        cardsContainer.appendChild(card.createCard());
                    });
                })
                .catch(error => console.error("Error fetching posts:", error));
        })
        .catch(error => console.error("Error fetching users:", error));

    cardsContainer.addEventListener("click", event => {
        if (event.target.classList.contains("delete-btn")) {
            const postId = event.target.dataset.postId;

            fetch(`${POSTS_API}/${postId}`, {
                method: "DELETE"
            })
                .then(response => {
                    if (response.status === 200) {
                        event.target.parentElement.remove();
                    }
                })
                .catch(error => console.error("Error deleting post:", error));
        }
    });
});